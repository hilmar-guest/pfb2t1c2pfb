The programs pfb2t1c and t1c2pfb can (re)convert PostScript Type1 fonts (pfb files) into the t1c format. During conversion no information is lost but the resulting t1c file is much more compressible.
